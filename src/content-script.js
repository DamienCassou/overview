/* eslint-env browser */
/* global browser, window */

function guid () {
  function _p8 (s) {
    var p = (Math.random().toString(16) + '000000000').substr(2, 8)
    return s ? '-' + p.substr(0, 4) + '-' + p.substr(4, 4) : p
  }

  return _p8() + _p8(true) + _p8(true) + _p8()
};

function isAboveFirstThird (node) {
  var rect = node.getBoundingClientRect()
  var viewHeight = window.innerHeight

  return rect.top < viewHeight / 3
}

function setNodeActiveState (nodes) {
  nodes.forEach((node) => { node.overviewActive = false })

  let lastAboveHalfScreen = nodes[0]
  for (let node of nodes) {
    if (!isAboveFirstThird(node)) {
      break
    }

    lastAboveHalfScreen = node
  }
  lastAboveHalfScreen.overviewActive = true
}

function nodeToHeading (node) {
  let id

  if (node.id) {
    id = node.id
  } else {
    id = guid()
    node.id = id
  }

  return {
    id: id,
    tag: node.tagName,
    text: node.textContent,
    active: node.overviewActive
  }
}

function getHeadings () {
  let nodes = document.body.querySelectorAll('h1, h2, h3, h4, h5, h6')

  if (nodes.length > 0) {
    setNodeActiveState(nodes)
  }

  let result = []

  nodes.forEach((node) => {
    result.push(nodeToHeading(node))
  })

  return Promise.resolve(result)
}

function scrollToId (id) {
  document.getElementById(id).scrollIntoView()
}

function messageReceived (message) {
  if (message.action === 'getHeadings') {
    return getHeadings()
  }

  if (message.action === 'scroll') {
    scrollToId(message.id)
    return Promise.resolve()
  }

  return Promise.reject(new Error('Unknown action'))
}

function handleScroll () {
  browser.runtime.sendMessage({action: 'updatedHeaders'})
}

window.addEventListener('scroll', handleScroll)
browser.runtime.onMessage.addListener(messageReceived)

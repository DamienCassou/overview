/* global browser */

/**
 * Replace content of container node by node `newContent`.
 */
function replaceContainerContent (newContent) {
  let parent = document.getElementById('container')

  while (parent.firstChild) {
    parent.firstChild.remove()
  }

  parent.appendChild(newContent)
}

function displayMessage (message) {
  let p = document.createElement('p')
  p.textContent = message
  replaceContainerContent(p)
  return p
}

async function getActiveTab () {
  return (await browser.tabs.query({currentWindow: true, active: true}))[0]
}

async function sendScript (tabId) {
  await browser.tabs.executeScript(tabId, {file: '/browser-polyfill.min.js'})
  await browser.tabs.executeScript(tabId, {file: '/content-script.js'})
}

async function sendScriptAndGetHeadings (tabId) {
  await sendScript(tabId)
  return browser.tabs.sendMessage(tabId, {action: 'getHeadings'})
}

async function getHeadings () {
  let tab = await getActiveTab()

  try {
    let headings = await browser.tabs.sendMessage(tab.id, {action: 'getHeadings'})
    return headings || sendScriptAndGetHeadings(tab.id)
  } catch (error) {
    return sendScriptAndGetHeadings(tab.id)
  }
}

async function scrollTabToId (id) {
  let tab = await getActiveTab()
  return browser.tabs.sendMessage(tab.id, {action: 'scroll', id})
}

function headingToNode (heading) {
  let anchor = document.createElement('a')
  anchor.addEventListener('click', () => scrollTabToId(heading.id))
  anchor.textContent = heading.text

  let node = document.createElement(heading.tag)
  node.appendChild(anchor)

  if (heading.active) {
    node.setAttribute('class', 'active')
  }

  return node
}

function displayTabHeadings (headings) {
  let div = document.createElement('div')

  headings.forEach((heading) => {
    div.appendChild(headingToNode(heading))
  })

  replaceContainerContent(div)
}

async function displaySidebarAsync () {
  let headings = await getHeadings()
  displayTabHeadings(headings)
}

function displaySidebar () {
  displaySidebarAsync()
    .catch((error) => displayMessage(error.message))
}

async function messageReceived (message, sender) {
  if (message.action !== 'updatedHeaders') {
    return
  }

  let activeTab = (await getActiveTab()).id
  let senderTab = sender.tab.id

  if (activeTab === senderTab) {
    displaySidebar()
  }
}

function init () {
  browser.runtime.onMessage.addListener(messageReceived)

  // Refresh sidebar when user chooses a different tab
  browser.tabs.onActivated.addListener(({tabId}) => {
    displaySidebar(tabId)
  })

  // Refresh sidebar when current tab is refreshed
  browser.tabs.onUpdated.addListener((tabId) => {
    displaySidebar(tabId)
  })

  // Immediately fill sidebar from active tab
  displaySidebar()
}

init()

WEBEXT_POLYFILL=node_modules/webextension-polyfill/dist/browser-polyfill.min.js
SRC_FILES=$(wildcard src/*) ${WEBEXT_POLYFILL}
FFOX_SRC_FILES=$(wildcard browser/firefox/*)
FFOX_BUILD_FILES=$(addprefix build/firefox/,$(notdir ${FFOX_SRC_FILES} ${SRC_FILES}))
CHRM_SRC_FILES=$(wildcard browser/chromium/*)
CHRM_BUILD_FILES=$(addprefix build/chromium/,$(notdir ${CHRM_SRC_FILES} ${SRC_FILES}))
INSTALL=install --mode=a+r-w -D --target-directory

##### General-purpose rules
.PHONY: default build firefox-build firefox-debug chromium-build
default: build
build: ${FFOX_BUILD_FILES} ${CHRM_BUILD_FILES}
build/%/browser-polyfill.min.js: ${WEBEXT_POLYFILL}
	${INSTALL} $(dir $@) $<

##### Firefox-specific rules
firefox-build: ${FFOX_BUILD_FILES}
build/firefox/%: src/%
	${INSTALL} $(dir $@) $<
build/firefox/%: browser/firefox/%
	${INSTALL} $(dir $@) $<
firefox-debug: ${FFOX_BUILD_FILES}
	web-ext run --source-dir build/firefox
overview.xpi: ${FFOX_BUILD_FILES}
	-rm -f $@
	cd build/firefox && zip ../../$@ ./*

##### Chromium-specific rules
chromium-build: ${CHRM_BUILD_FILES}
build/chromium/%: src/%
	${INSTALL} $(dir $@) $<
build/chromium/%: browser/chromium/%
	${INSTALL} $(dir $@) $<
